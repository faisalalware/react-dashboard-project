import React from 'react'
import LoginContainer from '../containers/Login/LoginContainer'

function Login() {
  return (
    <LoginContainer />
  )
}

export default Login