import React from 'react'
import TablesContainer from '../containers/Tables/TablesContainer'

function Tables() {
  return (
    <TablesContainer />
  )
}

export default Tables