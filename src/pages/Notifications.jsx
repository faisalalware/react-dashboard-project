import React from 'react'
import NotificationsContainer from '../containers/Notifications/NotificationsContainer'

function Notifications() {
  return (
    <NotificationsContainer />
  )
}

export default Notifications