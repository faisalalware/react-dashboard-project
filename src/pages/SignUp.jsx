import React from 'react'
import SignUpContainer from '../containers/SignUp/SignUpContainer'

function SignUp() {
  return (
    <SignUpContainer />
  )
}

export default SignUp