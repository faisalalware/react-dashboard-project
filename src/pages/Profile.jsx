import React from 'react'
import ProfileContainer from '../containers/Profile/ProfileContainer'

function Profile() {
  return (
    <ProfileContainer />
  )
}

export default Profile