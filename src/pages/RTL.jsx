import React from 'react'
import RTLContainer from '../containers/RTL/RTLContainer'

function RTL() {
  return (
    <RTLContainer />
  )
}

export default RTL