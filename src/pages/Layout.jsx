import React from 'react'
import LayoutContainer from '../containers/Layout/LayoutContainer'

function Layout() {
  return (
    <LayoutContainer />
  )
}

export default Layout