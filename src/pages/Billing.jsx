import React from 'react'
import BillingContainer from '../containers/Billing/BillingContainer'

function Billing() {
  return (
    <BillingContainer />
  )
}

export default Billing