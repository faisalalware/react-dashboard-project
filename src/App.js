import React, { useEffect, useState } from "react";

import './App.css';
// import LayoutContainer from './containers/Layout/LayoutContainer';

import {
  Routes, Route, Redirect, Navigate,
  useNavigate, useLocation, useParams
} from "react-router-dom";

import Dashboard from './pages/Dashboard'
import Billing from './pages/Billing'
import Layout from './containers/Layout'
import Login from './pages/Login'
import SignUp from './pages/SignUp'
import Notifications from './pages/Notifications'  
import Profile from './pages/Profile'
import RTL from './pages/RTL'
import Tables from './pages/Tables'

// Public Routs list
// const PUBLIC_ROUTES = ["/dashboard"];

function App() {
  // const [location, setLocation] = useState({})

	// const _location = useLocation();
	// const params = useParams();
	// const navigate = useNavigate();
	
	// const loggedIn = true;

	// useEffect(() => {
	// 	console.log("App didMount");
	// 	validateRoute();
	// });
	
	// useEffect(() =>{
	// 	console.log("App didUpdate");
	// 	setLocation({..._location, params, navigate});
	// }, [_location.pathname]);

	// const validateRoute = () => {
	// 	let isPublic = PUBLIC_ROUTES.includes(location.pathname);
	// 	let navigatePath = "";

	// 	if (loggedIn) {
	// 		if (!isPublic) {
	// 			navigatePath = "/dashboard";
	// 		}
	// 	} else {
	// 		if (isPublic) {
	// 			navigatePath = "/login";
	// 		}
	// 	}

	// 	if (navigatePath) {
	// 		// navigate(navigatePath);
	// 	}
	// }

  return (
    <div>
      <Routes>
        {/* without Layout */}
        {/* <Route index element={<Navigate to="/login" replace />} /> */}
        <Route
          path="/signin"
          element={<Login />}
        />

        <Route
          path="/signup"
          element={<SignUp />}
        />

        {/* With Layout */}
        <Route path="/" element={<Layout />}>
          <Route index element={<Navigate to="/dashboard" replace />} />

          <Route
            path="/dashboard"
            element={<Dashboard />}
            props={{ name: "value from dashboard" }}
          />
          <Route
            path="/tables"
            element={<Tables />}
          />
          <Route
            path="/billing"
            element={<Billing />}
          />
          {/* <Route
            path="/rtl"
            element={<RTL />}
          /> */}
          <Route
            path="/notifications"
            element={<Notifications />}
          />
          <Route
            path="/profile"
            element={<Profile />}
          />
        </Route>

        {/* path Not found */}
        <Route
          path="*"
          element={<>Page Not Found</>}
        />
      </Routes>
      {/* <LayoutContainer /> */}
    </div>
  );
}

export default App;
