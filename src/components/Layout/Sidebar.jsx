import React from 'react'
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { styled } from '@mui/material/styles';
import Text from '../common/Text';
import { Button } from '@mui/material';
import DashboardIcon from '@mui/icons-material/Dashboard';
import TableViewIcon from '@mui/icons-material/TableView';
import ReceiptLongIcon from '@mui/icons-material/ReceiptLong';
// import FormatTextdirectionRToLIcon from '@mui/icons-material/FormatTextdirectionRToL';
import NotificationsIcon from '@mui/icons-material/Notifications';
import PersonIcon from '@mui/icons-material/Person';
import LoginIcon from '@mui/icons-material/Login';
import AssignmentIcon from '@mui/icons-material/Assignment';
import CloseIcon from '@mui/icons-material/Close';

// Routing
import { useLocation, useNavigate, Link } from "react-router-dom";

const drawerWidth = 250;

// List item button color on hover
const StyledList = styled(List)({
  '& .MuiListItemButton-root:hover': {
    backgroundColor: '#616166',
  },
});

const routes = {
  dashboard: `/dashboard`,
  tables: `/tables`,
  billing: `/billing`,
  // rtl: `/rtl`,
  notifications: `/notifications`,
  profile: `/profile`,
  signin: `/signin`,
  signup: `/signup`,
};

const Sidebar = (props) => {

  const navigate = useNavigate();
  const location = useLocation();

  const isActiveRoute = (pathname) => {
    // console.log("This => ", pathname ," = ",window.location.pathname === pathname, location);
    return location.pathname === pathname
  }

  const handleSidebarRedirection = (pathname) => {
    navigate(pathname);
  };

  const menu = [
    {
      text: "Dashboard",
      icon: <DashboardIcon />,
      onClick: () => handleSidebarRedirection(routes.dashboard),
      isActive: isActiveRoute(routes.dashboard),
    },
    {
      text: "Tables",
      icon: <TableViewIcon />,
      onClick: () => handleSidebarRedirection(routes.tables),
      isActive: isActiveRoute(routes.tables),
    },
    {
      text: "Billing",
      icon: <ReceiptLongIcon />,
      onClick: () => handleSidebarRedirection(routes.billing),
      isActive: isActiveRoute(routes.billing),
    },
    // {
    //   text: "RTL",
    //   icon: <FormatTextdirectionRToLIcon />,
    //   onClick: () => handleSidebarRedirection(routes.rtl),
    //   isActive: isActiveRoute(routes.rtl),
    // },
    {
      text: "Notifications",
      icon: <NotificationsIcon />,
      onClick: () => handleSidebarRedirection(routes.notifications),
      isActive: isActiveRoute(routes.notifications),
    },
    {
      text: "Profile",
      icon: <PersonIcon />,
      onClick: () => handleSidebarRedirection(routes.profile),
      isActive: isActiveRoute(routes.profile),
    },
    {
      text: "Sign In",
      icon: <LoginIcon />,
      onClick: () => handleSidebarRedirection(routes.signin),
      isActive: isActiveRoute(routes.signin),
    },
    {
      text: "Sign Up",
      icon: <AssignmentIcon />,
      onClick: () => handleSidebarRedirection(routes.signup),
      isActive: isActiveRoute(routes.signup),
    }
  ];

  return (
    <Drawer
      PaperProps={{
        sx: {
          color: "#fff",
          backgroundColor: "#313136",
          padding: "10px",
          margin: "15px",
          borderRadius: "10px",
          height: 'calc(100% - 30px)',
        }
      }}
      sx={{
        display: { xs: 'none', sm: 'block' },
        width: drawerWidth,
        flexShrink: 0,
        '& .MuiDrawer-paper': {
          width: drawerWidth,
          boxSizing: 'border-box',
        },
        padding: "15px",
        backgroundColor: "grey.100"
      }}
      variant="permanent"
      anchor="left"
    >
      <CloseIcon
        sx={{
          display: { xs: 'block', sm: 'none' },
          width: "18px",
          position: "absolute",
          top: "10px",
          right: "10px",
          color: "#7b809a"
        }} />
      <Link to={routes.dashboard} style={{ textDecoration: "none" }}>
        <Box sx={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
          padding: "20px 0",
        }}>
          <img src="/Images/creative-tim.png" alt="logo" style={{
            width: "32px",
          }} />
          <Text variant="small"
            sx={{
              color: "#fff",
              fontWeight: "bold",
              fontSize: "14px",
              marginLeft: "5px"
            }}>Material Dashboard 2</Text>
        </Box>
      </Link>
      <Divider variant='fullWidth'
        sx={{
          backgroundColor: "#fff",
          opacity: "0.2"
        }} />
      <StyledList
        sx={{
          padding: "15px 0"
        }}>
        {menu.map((item, index) => {
          const {
            text,
            icon,
            isActive = false,
            onClick = () => { }, } = item;
          return (
            <ListItem key={index}
              sx={{
                padding: "3px 5px",
              }}>
              <ListItemButton
                onClick={onClick}
                sx={{
                  borderRadius: "5px",
                  backgroundColor: isActive ? "#2881eb" : "transparent",
                  '& .MuiListItemButton-root:hover': {
                    backgroundColor: '#616166',
                  }
                }}>
                {icon && <ListItemIcon
                  sx={{
                    minWidth: "40px",
                    color: "#fff"
                  }}>
                  {icon}
                </ListItemIcon>}
                <ListItemText
                  primary={text}
                  disableTypography
                  sx={{
                    fontSize: "15px"
                  }} />
              </ListItemButton>
            </ListItem>
          )
        })}
      </StyledList>
      <a href="https://www.creative-tim.com/product/material-dashboard-pro-react?_ga=2.75088277.664970093.1706529247-1679500540.1702361113" target='_blank' rel="noreferrer">
        <Button
          variant='contained'
          sx={{
            position: "absolute",
            bottom: "15px",
            left: "18px",
            backgroundColor: "#2881eb",
            fontWeight: 600,
            fontSize: 11,
            padding: "10px 55px",
            borderRadius: "8px",
            color: "#fff",
            letterSpacing: 0
          }}>
          Upgrade to Pro</Button>
      </a>
    </Drawer>
  )
}

export default React.memo(Sidebar);