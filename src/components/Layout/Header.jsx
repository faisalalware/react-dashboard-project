import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import MenuIcon from "@mui/icons-material/Menu";
import AccountCircle from "@mui/icons-material/AccountCircle";
import InputAdornment from "@mui/material/InputAdornment";
import SettingsIcon from "@mui/icons-material/Settings";
import NotificationsIcon from "@mui/icons-material/Notifications";
import HomeIcon from "@mui/icons-material/Home";
import MenuItem from "@mui/material/MenuItem";
import Menu from "@mui/material/Menu";
import Icon from "@mui/material/Icon";
import { Stack } from "@mui/material";
import TextField from "@mui/material/TextField";
import Text from "../common/Text";
import Link from "@mui/material/Link";

// const Search = styled("div")(({ theme }) => ({
//   position: "relative",
//   borderRadius: theme.shape.borderRadius,
//   backgroundColor: alpha(theme.palette.common.white, 0.15),
//   "&:hover": {
//     backgroundColor: alpha(theme.palette.common.white, 0.25),
//   },
//   border: "0.1px solid",
//   borderColor: "rgb(123, 128, 154)",
//   marginLeft: 0,
//   width: "100%",
//   [theme.breakpoints.up("sm")]: {
//     marginLeft: theme.spacing(1),
//     width: "auto",
//   },
// }));

// const SearchIconWrapper = styled("div")(({ theme }) => ({
//   padding: theme.spacing(0, 2),
//   height: "100%",
//   position: "absolute",
//   pointerEvents: "none",
//   display: "flex",
//   alignItems: "center",
//   justifyContent: "center",
// }));

// const StyledInputBase = styled(InputBase)(({ theme }) => ({
//   color: "inherit",
//   "& .MuiInputBase-input": {
//     padding: theme.spacing(1, 1, 1, 0),
//     // vertical padding + font size from searchIcon
//     paddingLeft: `calc(1em + ${theme.spacing(0.5)})`,
//     transition: theme.transitions.create("width"),
//     width: "100%",
//     [theme.breakpoints.up("sm")]: {
//       width: "12ch",
//       "&:focus": {
//         width: "20ch",
//       },
//     },
//   },
// }));

const Header = (props) => {
  const { title = "" } = props;
  const [notification, setNotification] = React.useState(null);

  const isMenuOpen = Boolean(notification);
  const handleNotificationOpen = (event) => {
    setNotification(event.currentTarget);
  };

  const handleNotificationClose = () => {
    setNotification(null);
    handleNotificationClose();
  };

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      notification={notification}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "left",
      }}
      open={isMenuOpen}
      onClose={handleNotificationClose}
    >
      <MenuItem onClick={handleNotificationClose} icon={<Icon>email</Icon>}>
        Check new messages
      </MenuItem>
      <MenuItem onClick={handleNotificationClose} icon={<Icon>podcasts</Icon>}>
        Manage Podcast sessions
      </MenuItem>
      <MenuItem
        onClick={handleNotificationClose}
        icon={<Icon>shopping_cart</Icon>}
      >
        Payment successfully completed
      </MenuItem>
    </Menu>
  );
  return (
    <AppBar
      position="sticky"
      color="transparent"
      sx={{
        borderRadius: "10px",
        marginTop: "18px",
        backdropFilter:"blur(20px)"
      }}
    >
      <Toolbar>
        <Stack
          sx={{
            flexGrow: "1",
          }}
        >
          <Stack direction="row">
            <IconButton
              size="small"
              edge="start"
              aria-label="open drawer"
              sx={{ mr: 0.5 }}
            >
              <InputAdornment sx={{ color: "rgb(52, 71, 103)" }}>
                <HomeIcon />
              </InputAdornment>
            </IconButton>
            <Typography
              noWrap
              component="div"
              sx={{
                // flexGrow: 1,
                display: {
                  xs: "none",
                  sm: "block",
                  color: "rgb(52, 71, 103)",
                  fontSize: "14px",
                  fontWeight: "300",
                  fontFamily: " Roboto, Helvetica, Arial, sans-serif",
                },
              }}
            >
              / {title}
            </Typography>
          </Stack>
          <Text sx={{ fontWeight: "bold", ml: 1 }}>{title}</Text>
        </Stack>
        <TextField
          id="outlined-textarea"
          label="Search Here"
          placeholder=""
          multiline
          size="small"
        />
        <Stack sx={{ px: 2 }} direction="row" spacing={0.5}>
          <Link href="/dashboard">
          <IconButton
            size="small"
            edge="end"
            aria-label="account of current user"
            aria-haspopup="true"
            color="inherit"
          >
            <AccountCircle sx={{ color: "rgb(123, 128, 154)" }} />
          </IconButton>
          </Link>
          <IconButton
            size="small"
            edge="end"
            color="inherit"
            aria-label="open drawer"
          >
            <MenuIcon sx={{ color: "rgb(123, 128, 154)" }} />
          </IconButton>

          <IconButton
            size="small"
            edge="end"
            aria-haspopup="true"
            color="inherit"
          >
            <SettingsIcon sx={{ color: "rgb(123, 128, 154)" }} />
          </IconButton>

          <IconButton
            size="small"
            color="inherit"
            onClick={handleNotificationOpen}
            aria-haspopup="true"
          >
            <NotificationsIcon sx={{ color: "rgb(123, 128, 154)" }} />
          </IconButton>
        </Stack>
      </Toolbar>
      {renderMenu}
    </AppBar>
  );
};

export default Header;
