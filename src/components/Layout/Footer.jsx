import { Stack, Link, Box } from '@mui/material'
import React from 'react'
import Text from '../common/Text'

export const Footer = () => {

    const links = {
        textDecoration: "none",
        padding: '0 15px',
        color: "#333",
        cursor: 'pointer'
    }

    return (
        <Stack sx={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            justifyContent: 'space-between',
            padding: '10px 0',
            margin: "15px 35px",
            // border: "1px solid #000"
        }}>
            <Stack sx={{
                textAlign: "center",
            }}>
                <Text variant="small">
                    &copy; 2022, made with 	&#10084; by <b>Creative Tim</b> for a better web.
                </Text>
            </Stack>
            <Stack
                sx={{
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    marginTop: { xs: 3, sm: 3, md: 3, lg: 0 },
                }}>
                <Link style={links} href="/">Creative Tim</Link>
                <Link style={links} href="/">About Us</Link>
                <Link style={links} href="/">Blog</Link>
                <Link style={links} href="/">License</Link>
            </Stack>
        </Stack>
    )
}

export default Footer