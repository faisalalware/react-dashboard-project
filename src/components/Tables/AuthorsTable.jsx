import React from "react";
import CardHeader from '../common/CardHeader'
// import { Stack } from '@mui/material'
import DashboardCard from "../common/DashboardCard";
// import MoreVertIcon from '@mui/icons-material/MoreVert';
// import Text from '../common/Text'
// import GitHubIcon from '@mui/icons-material/GitHub';

import {
  // Card,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  // Paper,
  Avatar,
  Grid,
  Typography,
} from "@mui/material";

const table = [
  {
    author: "John Micheal",
    src: "/Images/Tables/img1.png",
    email: "john@creative-tim.com",
    function: "Manager",
    sub_function: "Organization",
    status: "online",
    employed: "23/09/17",
    action: "Edit",
  },
  {
    author: "Alexa Liras",
    src: "/Images/Tables/img2.png",
    email: "john@creative-tim.com",
    function: "Manager",
    sub_function: "Developer",
    status: "offline",
    employed: "23/09/17",
    action: "Edit",
  },
  {
    author: "Laurent Perrier",
    src: "/Images/Tables/img1.png",
    email: "john@creative-tim.com",
    function: "Programmer",
    sub_function: "Projects",
    status: "online",
    employed: "4/01/22",
    action: "Edit",
  },
  {
    author: "Michael Levi",
    email: "john@creative-tim.com",
    src: "/Images/Tables/img1.png",
    function: "Programmer",
    sub_function: "Developer",
    status: "online",
    employed: "4/01/22",
    action: "Edit",
  },
  {
    author: "Richard Gran",
    src: "/Images/Tables/img2.png",
    email: "richard@creative-tim.com",
    function: "Programmer",
    sub_function: "Executive",
    status: "offline",
    employed: "4/01/22",
    action: "Edit",
  },
  {
    author: "Miriam Eric",
    src: "/Images/Tables/img2.png",
    email: "miriam@creative-tim.com",
    function: "Programmer",
    sub_function: "Developer",
    status: "offline",
    employed: "4/01/22",
    action: "Edit",
  },
];


function AuthorsTable() {
  return (
    <>
      <DashboardCard>
        <CardHeader title="Author's Table" />
        <TableContainer>
          <Table
            sx={{
              background: "rgb(255, 255, 255)",
              color: "rgb(123, 128, 154)",
              opacity: "0.7",
              fontFamily: "Roboto, Helvetica, Arial, sans-serif",
              borderRadius: '20px'
            }}
            aria-label="simple table"
          >
            <TableHead>
              <TableRow
                sx={{
                  textTransform: "uppercase",
                  fontSize: "10px",
                }}
              >
                <TableCell
                  sx={{ fontSize: "10px", padding: "55px 35px 15px 30px" }}
                >
                  Author
                </TableCell>
                <TableCell
                  sx={{ fontSize: "10px", padding: "55px 35px 15px 20px" }}
                >
                  Function
                </TableCell>
                <TableCell
                  sx={{ fontSize: "10px", padding: "55px 35px 15px 30px" }}
                >
                  Status
                </TableCell>
                <TableCell
                  sx={{ fontSize: "10px", padding: "55px 0px 15px 15px" }}
                >
                  Employed
                </TableCell>
                <TableCell
                  sx={{ fontSize: "10px", padding: "55px 0px 15px 10px" }}
                >
                  Action
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {table.map((row) => (
                <TableRow
                  key={row.name}
                  sx={{
                    "&:last-child td, &:last-child th": { border: 0 },
                  }}
                >
                  <TableCell>
                    <Grid container>
                      <Grid item lg={2} xs={2}>
                        <Avatar src={row.src}/>
                      </Grid>
                      <Grid item lg={10} xs={4}>
                        <Typography sx={{ fontWeight: "700", color: "black" }}>
                          {row.author}
                        </Typography>
                        <Typography variant="h7">{row.email}</Typography>
                      </Grid>
                    </Grid>
                  </TableCell>
                  <TableCell>
                    <Grid container>
                      <Grid item lg={2} xs={2}>
                        <Typography
                          sx={{
                            fontWeight: "700",
                            color: "black",
                            fontSize: "13px",
                          }}
                        >
                          {row.function}
                        </Typography>
                        <Typography variant="h7">{row.sub_function}</Typography>
                      </Grid>
                    </Grid>
                  </TableCell>
                  <TableCell>
                    <Typography
                      sx={{
                        width: "50px",
                        textTransform: "uppercase",
                        fontWeight: "700",
                        fontSize: "11px",
                        color: "white",
                        backgroundColor: "red",
                        borderRadius: "8px",
                        padding: "5px 10px",
                        textAlign: "center",
                        //     display: 'inline-block'
                      }}
                      style={{
                        backgroundColor:
                          (row.status === "online" && "green") ||
                          (row.status === "offline" && "black"),
                      }}
                    >
                      {row.status}
                    </Typography>
                  </TableCell>
                  <TableCell
                    sx={{ fontWeight: "800", fontSize: "12px"}}
                  ><Grid item xs={2}>
                    {row.employed}
                    </Grid>
                  </TableCell>
                  <TableCell
                    sx={{ fontWeight: "800", fontSize: "12px"}}
                  >
                    {row.action}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </DashboardCard>
    </>
  );
}

export default AuthorsTable;
