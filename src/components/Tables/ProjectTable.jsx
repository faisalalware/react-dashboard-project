import React from "react";
import CardHeader from '../common/CardHeader'
import DashboardCard from "../common/DashboardCard";
import MoreVertIcon from '@mui/icons-material/MoreVert';
// import Text from '../common/Text'

import {
//   Card,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  // Paper,
  Avatar,
  Grid,
  Typography,
} from "@mui/material";


const table1 = [
  {
    project: "Asana",
    src: '/Images/Tables/asaana.png',
    budget:'$2500',
    status: "working",
    completion: '/Images/Tables/completion60.png',
    action: "",
  },
  {
    project: "Github",
    src:'/Images/Tables/github.png',
    budget:'$5000',
    status: "done",
    completion: '/Images/Tables/comp2.png',
    action: "",
  },
  {
    project: "Atlassian",
    src: '/Images/Tables/atlassian.png',
    budget:'$3400',
    status: "cancelled",
    completion: '/Images/Tables/comp3.png',
    action: "",
  },
  {
    project: "Spotify",
    src: '/Images/Tables/spotify.png',
    budget:'$14000',
    status: "working",
    completion: '/Images/Tables/comp4.png',
    action: "",
  },
  {
    project: "Slack",
    src: '/Images/Tables/slack.png',
    budget:'$1000',
    status: "cancelled",
    completion: '/Images/Tables/comp5.png',
    action: "",
  },
  {
    project: "Invension",
    src: '/Images/Tables/inversion.png',
    budget:'$2300',
    status: "done",
    completion: '/Images/Tables/comp2.png',
    action: "",
  },
];

function ProjectTable() {
  return (
    <>
      <DashboardCard>

        <CardHeader title="Project's Table"/>
       
        <TableContainer sx={{
            marginBottom: '-25px'
        }}>
          <Table
            sx={{
              background: "rgb(255, 255, 255)",
              color: "rgb(123, 128, 154)",
              opacity: "0.7",
              fontFamily: "Roboto, Helvetica, Arial, sans-serif",
              
            }}
            aria-label="simple table"
          >
            <TableHead>
              <TableRow
                sx={{
                  textTransform: "uppercase",
                  fontSize: "10px",
                }}
              >
                <TableCell
                  sx={{ fontSize: "10px", padding: "55px 35px 15px 30px" }}
                >
                  Project
                </TableCell>
                <TableCell
                  sx={{ fontSize: "10px", padding: "55px 35px 15px 15px" }}
                >
                  Budget
                </TableCell>
                <TableCell
                  sx={{ fontSize: "10px", padding: "55px 35px 15px 20px" }}
                >
                  Status
                </TableCell>
                <TableCell
                  sx={{ fontSize: "10px", padding: "55px 0 15px 54px" }}
                >
                  Completion
                </TableCell>
                <TableCell
                  sx={{ fontSize: "10px", padding: "55px 0px 15px 10px" }}
                >
                  Action
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {table1.map((row) => (
                <TableRow
                  key={row.name}
                  sx={{
                    "&:last-child td, &:last-child th": { border: 0 },
                  }}
                >
                  <TableCell>
                    <Grid container>
                      <Grid item lg={3}>
                        <Avatar alt={row.name} src={row.src} />
                      </Grid>
                      <Grid item lg={9}>
                        <Typography sx={{ fontWeight: "700", color: "black", paddingTop: '8px' }}>
                          {row.project}
                        </Typography>
                      </Grid>
                    </Grid>
                  </TableCell>
                  <TableCell
                    sx={{ fontWeight: "bold", fontSize: "14px",  color: "rgb(52, 71, 103)" , opacity: 1 }}
                  >
                    {row.budget}
                  </TableCell>
                  <TableCell
                    sx={{ fontWeight: "bold", fontSize: "12px",  color: "rgb(52, 71, 103)"  }}
                  >
                    {row.status}
                  </TableCell>
                  <TableCell
                    sx={{ fontWeight: "800", fontSize: "12px",  color: "rgb(52, 71, 103)"  }}
                  >
                    <img src={row.completion} alt="" width="170px"/>                    
                  </TableCell>
                  <TableCell
                    sx={{ fontWeight: "800", fontSize: "12px",  color: "rgb(52, 71, 103)"  }}
                  >
                    < MoreVertIcon/>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </DashboardCard>
    </>
  );
}

export default ProjectTable;
