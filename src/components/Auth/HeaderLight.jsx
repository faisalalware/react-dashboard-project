import React from 'react'
import { Stack } from '@mui/material'
import Text from '../common/Text'
import Link from '@mui/material/Link'
import Button from '@mui/material/Button'
import IconButton from '@mui/material/IconButton'
import MenuIcon from '@mui/icons-material/Menu';
import PersonIcon from '@mui/icons-material/Person';
import DataSaverOffIcon from '@mui/icons-material/DataSaverOff';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import KeyIcon from '@mui/icons-material/Key';

function HeaderLight() {

    const links = {
        textDecoration: "none",
        padding: '0 15px',
        color: "#000",
        cursor: 'pointer',
        fontWeight: "bold"
    }

    const text ={
        marginLeft: "5px"
    }

    return (
        <Stack sx={{
            margin:"auto"
        }}>
            <Stack sx={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
                borderRadius: "10px",
                padding: "10px 15px",
                margin: "20px 0",
                width: "70vw",
            }}>
                <Stack>
                    <Link style={{ textDecoration: "none" }} href="/dashboard">
                    <Text sx={{ fontWeight: "bold", color: "#000" }}>Material Dashboard 2</Text>
                    </Link>
                </Stack>
                <Stack sx={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-evenly",
                    display: { xs: 'none', sm: 'none', md: 'block' }
                }}>
                    <Link style={links} href="/dashboard">
                        <IconButton>
                            <DataSaverOffIcon />
                            <Text style={text}>Dashboard</Text>
                        </IconButton>
                    </Link>
                    <Link style={links} href="/profile">
                        <IconButton>
                            <PersonIcon />
                            <Text style={text}>Profile</Text>
                        </IconButton>
                    </Link>
                    <Link style={links} href="/signup">
                        <IconButton>
                            <AccountCircleIcon />
                            <Text style={text}>Sign Up</Text>
                        </IconButton>
                    </Link>
                    <Link style={links} href="/signin">
                        <IconButton>
                            <KeyIcon />
                            <Text style={text}>Sign In</Text>
                        </IconButton>
                    </Link>
                </Stack>
                <Stack sx={{
                    display: { xs: 'none', sm: 'none', md: 'block' }
                }}>
                    <Button variant='contained' sx={{ fontSize: 12, fontWeight: "bold", borderRadius: "10px", color: "#fff" }}>Free Download</Button>
                </Stack>
                <Stack sx={{
                    display: { xs: 'block', sm: 'block', md: 'none' }
                }}>
                    <IconButton>
                        <MenuIcon />
                    </IconButton>
                </Stack>
            </Stack>
        </Stack>
    )
}

export default HeaderLight