import React from 'react'
import { Stack } from '@mui/material'
import Text from '../common/Text'
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import Link from '@mui/material/Link';
import Card from '@mui/material/Card';
import Paper from '@mui/material/Paper';
import FacebookIcon from '@mui/icons-material/Facebook';
import GitHubIcon from '@mui/icons-material/GitHub';
import GoogleIcon from '@mui/icons-material/Google';

function Login() {
  return (
    <Stack sx={{
      margin: "auto"
    }}>
      <Stack sx={{
        padding: "50px 20px",
        borderRadius: "10px",
        backgroundColor: "#fff",
        width: "390px"
      }}>
        <Paper sx={{
          marginTop: '-80px',
          borderRadius: '8px',
          zIndex: 1111
        }}
          elevation={7}>
          <Card sx={{
            background: 'linear-gradient(195deg, rgb(73, 163, 241), rgb(26, 115, 232))',
            borderRadius: '8px',
            padding: '30px 20px',
            color: 'white',
            fontSize: '18px',
            fontWeight: '700',
            textAlign: "center"
          }}>
            <Stack>
              <Text sx={{
                fontWeight: "bold",
                fontSize: 23
              }}>Sign In</Text>
            </Stack>
            <Stack sx={{
              flexDirection: "row",
              justifyContent: "space-evenly",
              alignItems: "center",
              mt:5
            }}>
              <FacebookIcon />
              <GitHubIcon />
              <GoogleIcon />
            </Stack>
          </Card>
        </Paper>
        <TextField
          id="outlined-textarea"
          label="Email"
          placeholder=""
          multiline
          size='small'
          sx={{
            mt: 5,
          }}
        />
        <TextField
          id="outlined-textarea"
          label="Password"
          placeholder=""
          multiline
          size='small'
          sx={{
            mt: 3,
          }}
        />
        <FormGroup sx={{ mt: 3 }}>
          <FormControlLabel control={<Switch defaultChecked />} label="Remember Me" />
        </FormGroup>
        <Button variant='contained' sx={{ mt: 3, fontWeight: "bold", }}>Sign In</Button>
        <Stack direction="row" justifyContent="center" sx={{
          mt: 4
        }}>
          <Text variant="small">Don't have an account?</Text>
          <Link href='/signup' sx={{
            ml: 1,
            textDecoration: "none",
            fontWeight: "bold",
            cursor: "pointer"
          }}>
            Sign Up</Link>
        </Stack>
      </Stack>
    </Stack>
  )
}

export default Login