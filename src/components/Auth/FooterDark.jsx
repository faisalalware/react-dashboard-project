import { Stack, Link, Box } from '@mui/material'
import React from 'react'
import Text from '../common/Text'

export const FooterDark = () => {

    const links = {
        textDecoration: "none",
        padding: '0 15px',
        color: "#000",
        cursor: 'pointer'
    }

    return (
        <Stack sx={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            justifyContent: 'space-around',
            padding: '20px 0',
            margin: "15px 35px",
        }}>
            <Stack sx={{
                textAlign: "center",
            }}>
                <Text variant="small" sx={{color: "#000"}}>
                    &copy; 2022, made with 	&#10084; by <b>Creative Tim</b> for a better web.
                </Text>
            </Stack>
            <Stack
                sx={{
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    marginTop: { xs: 3, sm: 3, md: 3, lg: 0 },
                }}>
                <Link style={links} href="/">Creative Tim</Link>
                <Link style={links} href="/">About Us</Link>
                <Link style={links} href="/">Blog</Link>
                <Link style={links} href="/">License</Link>
            </Stack>
        </Stack>
    )
}

export default FooterDark