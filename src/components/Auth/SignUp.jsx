import React from 'react'
import { Stack } from '@mui/material'
import Text from '../common/Text'
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Card from '@mui/material/Card';
import Paper from '@mui/material/Paper';

function SignUp() {
  return (
    <Stack sx={{
      margin: "auto"
    }}>
      <Stack sx={{
        padding: "50px 20px",
        borderRadius: "10px",
        backgroundColor: "#fff",
        width: "390px"
      }}>
        <Paper sx={{
          marginTop: '-80px',
          borderRadius: '8px',
          zIndex: 1111
        }}
          elevation={7}>
          <Card sx={{
            background: 'linear-gradient(195deg, rgb(73, 163, 241), rgb(26, 115, 232))',
            borderRadius: '8px',
            padding: '30px 20px',
            color: 'white',
            fontSize: '18px',
            fontWeight: '700',
            textAlign: "center"
          }}>
            <Stack>
              <Text sx={{
                fontWeight: "bold",
                fontSize: 23
              }}>Join us today</Text>
              <Text sx={{
                fontSize: 15,
                mt: 1
              }}>Enter your email and password to register</Text>
            </Stack>
          </Card>
        </Paper>
        <TextField id="standard-basic" label="Name" variant="standard" sx={{ mt: 5 }} />
        <TextField id="standard-basic" label="Email" variant="standard" sx={{ mt: 3 }} />
        <TextField id="standard-basic" label="Password" variant="standard" sx={{ mt: 3 }} />
        <FormGroup sx={{ mt: 3 }}>
          <Stack sx={{
            flexDirection: "row",
            alignItems: "center"
          }}>
            <FormControlLabel control={<Checkbox defaultChecked />} label="I agree the" />
            <Link sx={{ marginLeft: "-10px", marginTop: "-3px", textDecoration: "none", fontWeight: "bold" }}>Terms and Conditions</Link>
          </Stack>
        </FormGroup>
        <Button variant='contained' sx={{ mt: 3, fontWeight: "bold", }}>Sign In</Button>
        <Stack direction="row" justifyContent="center" sx={{
          mt: 4
        }}>
          <Text variant="small">Already have an account?</Text>
          <Link href='/signin' sx={{
            ml: 1,
            textDecoration: "none",
            fontWeight: "bold",
            cursor: "pointer"
          }}>
            Sign In</Link>
        </Stack>
      </Stack>
    </Stack>
  )
}

export default SignUp