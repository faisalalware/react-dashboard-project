import React from "react";
import { Stack } from "@mui/material";
import DashboardCard from "../common/DashboardCard";
import Text from "../common/Text";
import Box from "@mui/material/Box";
import { Grid, Typography } from "@mui/material";
import Avatar from "@mui/material/Avatar";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";

import EmailIcon from "@mui/icons-material/Email";
import SettingsIcon from "@mui/icons-material/Settings";
import HomeIcon from "@mui/icons-material/Home";
import PlatformSettings from "./PlatformSettings";
import ProfileInformation from "./ProfileInformation";
import Conversations from "./Conversations";
import Projects from "./Projects";
import ProjectCard from "./ProjectCard";

function Profile() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <Stack>
      <Text
        variant={"h6"}
        sx={{
          fontWeight: "700",
          fontSize: "1rem",
          fontFamily: "Roboto, Helvetica, Arial, sans-serif",
          color: "rgb(52, 71, 103)",
        }}
      >
        Profile
      </Text>
      <Box position="relative" mb={5}>
        <Box
          display="flex"
          alignItems="center"
          position="relative"
          minHeight="18.75rem"
          borderRadius="15px"
          sx={{
            backgroundImage: "url(/Images/Profile/bg-profile.jpeg)",
            backgroundSize: "cover",
            backgroundPosition: "50%",
            overflow: "hidden",
            opacity: "0.6",
          }}
        />
        <Stack>
          <DashboardCard
            sx={{
              position: "relative",
              mt: -8,
              mx: 3,
              // py: 2,
              // px: 2,
            }}
          >
            <Grid container spacing={3} alignItems="center">
              <Grid item>
                <Avatar
                  alt="Bruce Mars"
                  src="/images/Profile/bruce-mars.jpg"
                  size="xl"
                  shadow="sm"
                  sx={{ width: 70, height: 70 }}
                />
              </Grid>
              <Grid item>
                <Box height="100%" mt={0.5} lineHeight={1}>
                  <Typography
                    variant="h5"
                    sx={{
                      fontSize: "1.25rem",
                      fontFamily: " Roboto, Helvetica, Arial, sans-serif",
                      color: "rgb(52, 71, 103)",
                      fontWeight: " 600",
                    }}
                  >
                    Richard Davis
                  </Typography>
                  <Typography
                    sx={{
                      fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                      fontSize: "0.875rem",
                      color: "rgb(123, 128, 154)",
                      fontWeight: "400",
                    }}
                  >
                    CEO / Co-Founder
                  </Typography>
                </Box>
              </Grid>

              <Grid item xs={12} md={6} lg={5} sx={{ ml: "auto" }}>
                <Tabs
                  value={value}
                  onChange={handleChange}
                  aria-label="icon tabs example"
                >
                  <Tab
                    fontSize="small"
                    icon={<HomeIcon sx={{ color: "rgb(52, 71, 103)" }} />}
                    iconPosition="start"
                    label="App"
                    variant="h6"
                    sx={{
                      color: "rgb(52, 71, 103)",
                      fontSize: "14px",
                    }}
                  />
                  <Tab
                    fontSize="small"
                    icon={<EmailIcon sx={{ color: "rgb(52, 71, 103)" }} />}
                    iconPosition="start"
                    label="Email"
                    variant="h6"
                    sx={{
                      color: "rgb(52, 71, 103)",
                      fontSize: "14px",
                    }}
                  />
                  <Tab
                    fontSize="small"
                    icon={<SettingsIcon sx={{ color: "rgb(52, 71, 103)" }} />}
                    iconPosition="start"
                    label="Settings"
                    variant="h6"
                    sx={{
                      color: "rgb(52, 71, 103)",
                      fontSize: "14px",
                    }}
                  />
                </Tabs>
              </Grid>
            </Grid>
            <Stack
              sx={{
                flexDirection: "row",
                flexWrap: "wrap",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Grid container spacing={1}>
                <Grid item xs={12} md={4}>
                  <PlatformSettings />
                </Grid>
                <Grid item xs={12} md={4}>
                  <ProfileInformation />
                </Grid>
                <Grid item xs={12} md={4}>
                  <Conversations />
                </Grid>
              </Grid>
              <Projects />
            </Stack>
          </DashboardCard>
        </Stack>
      </Box>
    </Stack>
  );
}

export default Profile;
