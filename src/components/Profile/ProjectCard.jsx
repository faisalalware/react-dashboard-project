import React from "react";
import { Stack } from "@mui/material";
import Text from "../common/Text";
import Button from "@mui/material/Button";
import Avatar from "@mui/material/Avatar";
import AvatarGroup from "@mui/material/AvatarGroup";

const ProjectCard = (props) => {
  const { src, project, title, data, button } = props;
  return (
    <Stack
      sx={{
        padding: "15px",
      }}
    >
      <Stack>
        <img
          src={src}
          //   alt="homeDecor1"
          style={{
            maxWidth: "100%",
            margin: "auto",
            borderRadius: "15px",
          }}
        />
      </Stack>
      <Stack
        sx={{
          width: "200px",
          mt: 3,
        }}
      >
        <Text
          variant="h6"
          sx={{
            fontFamily: "Roboto, Helvetica, Arial, sans-serif",
            fontSize: "0.875rem",
            color: "rgb(123, 128, 154)",
            fontWeight: "400",
          }}
        >
          {project}
        </Text>
        <Text
          sx={{
            mt: 2,
            fontWeight: "700",
            fontSize: "1.2rem",
            fontFamily: "Roboto, Helvetica, Arial, sans-serif",
            color: "rgb(52, 71, 103)",
          }}
        >
          {title}
        </Text>
        <Text
          sx={{
            mt: 2,
            fontFamily: "Roboto, Helvetica, Arial, sans-serif",
            fontSize: "0.875rem",
            color: "rgb(123, 128, 154)",
            fontWeight: "400",
          }}
        >
          {data}
        </Text>
      </Stack>
      <Stack
        sx={{
          flexDirection: "row",
          alignItems: "center",
          mt: 3,
        }}
      >
        <Button
          variant="outlined"
          sx={{ fontSize: "0.75rem", borderRadius: "0.5rem", minWidth: "64px" }}
        >
          {button}
        </Button>
        <AvatarGroup max={4}>
          <Avatar
            src="/Images/Tables/img2.png"
            sx={{ width: "20px", height: "20px" }}
          />
          <Avatar
            src="/Images/Tables/img1.png"
            sx={{ width: "20px", height: "20px" }}
          />
          <Avatar
            src="/Images/Tables/img2.png"
            sx={{ width: "20px", height: "20px" }}
          />
          <Avatar
            src="/Images/Tables/img2.png"
            sx={{ width: "20px", height: "20px" }}
          />
        </AvatarGroup>
      </Stack>
    </Stack>
  );
};

export default ProjectCard;
