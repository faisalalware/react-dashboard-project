import React from "react";
import { Stack, Typography, Box } from "@mui/material";
import Divider from "@mui/material/Divider";
import Avatar from "@mui/material/Avatar";
import Text from "../common/Text";
import Link from "@mui/material/Link";

function Conversations() {
  return (
    <Stack
      divider={<Divider orientation="vertical" sx={{ mx: 0 }} />}
      spacing={1} marginTop={"25px"}
    >
      <Box>
        <Typography
          variant={"h6"}
          textTransform="capitalize"
          sx={{
            fontWeight: "700",
            fontSize: "1rem",
            fontFamily: "Roboto, Helvetica, Arial, sans-serif",
            color: "rgb(52, 71, 103)",
          }}
        >
          Conversations
        </Typography>

        <Stack
          sx={{
            flexDirection: "column",
            mt: 5,
          }}
        >
          <Stack
            sx={{
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Stack>
              <Avatar alt=" Sophie B." src="/Images/Tables/img2.png" />
            </Stack>
            <Stack
              sx={{
                width: "150px",
                ml: 2,
              }}
            >
              <Text
                variant="h6"
                sx={{
                  fontWeight: "600",
                  fontSize: "0.875rem",
                  fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                  color: "rgb(52, 71, 103)",
                }}
              >
                Sophie B.
              </Text>
              <Text sx={{ fontSize: 14, color: "rgb(123, 128, 154)" }}>
                Hi! I need more information..
              </Text>
            </Stack>
            <Stack>
              <Link
                sx={{
                  textDecoration: "none",
                  fontWeight: "bold",
                  textTransform: "uppercase",
                }}
              >
                Reply
              </Link>
            </Stack>
          </Stack>
          <Stack
            sx={{
              flexDirection: "row",
              alignItems: "center",
              mt: 2,
            }}
          >
            <Stack>
              <Avatar alt="Nick Daniel" src="/images/Profile/bruce-mars.jpg" />
            </Stack>
            <Stack
              sx={{
                width: "150px",
                ml: 2,
              }}
            >
              <Text
                variant="h6"
                sx={{
                  fontWeight: "600",
                  fontSize: "0.875rem",
                  fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                  color: "rgb(52, 71, 103)",
                }}
              >
                Nick Daniel
              </Text>
              <Text sx={{ fontSize: 14, color: "rgb(123, 128, 154)" }}>
                Hi! I need more information..
              </Text>
            </Stack>
            <Stack>
              <Link
                sx={{
                  textDecoration: "none",
                  fontWeight: "bold",
                  textTransform: "uppercase",
                }}
              >
                Reply
              </Link>
            </Stack>
          </Stack>
          <Stack
            sx={{
              flexDirection: "row",
              alignItems: "center",
              mt: 2,
            }}
          >
            <Stack>
              <Avatar alt="Anne" src="/Images/Tables/img1.png" />
            </Stack>
            <Stack
              sx={{
                width: "150px",
                ml: 2,
              }}
            >
              <Text
                variant="h6"
                sx={{
                  fontWeight: "600",
                  fontSize: "0.875rem",
                  fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                  color: "rgb(52, 71, 103)",
                }}
              >
                Anne Marie
              </Text>
              <Text sx={{ fontSize: 14, color: "rgb(123, 128, 154)" }}>
                Awesome work, can you..
              </Text>
            </Stack>
            <Stack>
              <Link
                sx={{
                  textDecoration: "none",
                  fontWeight: "bold",
                  textTransform: "uppercase",
                }}
              >
                Reply
              </Link>
            </Stack>
          </Stack>
          <Stack
            sx={{
              flexDirection: "row",
              alignItems: "center",
              mt: 2,
            }}
          >
            <Stack>
              <Avatar alt="Ivanna" src="/Images/Tables/img2.png" />
            </Stack>
            <Stack
              sx={{
                width: "150px",
                ml: 2,
              }}
            >
              <Text
                variant="h6"
                sx={{
                  fontWeight: "600",
                  fontSize: "0.875rem",
                  fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                  color: "rgb(52, 71, 103)",
                }}
              >
                Ivanna
              </Text>
              <Text sx={{ fontSize: 14, color: "rgb(123, 128, 154)" }}>
                About files I can..
              </Text>
            </Stack>
            <Stack>
              <Link
                sx={{
                  textDecoration: "none",
                  fontWeight: "bold",
                  textTransform: "uppercase",
                }}
              >
                Reply
              </Link>
            </Stack>
          </Stack>

          <Stack
            sx={{
              flexDirection: "row",
              alignItems: "center",
              mt: 2,
            }}
          >
            <Stack>
              <Avatar alt="Peterson" src="/images/Profile/bruce-mars.jpg" />
            </Stack>
            <Stack
              sx={{
                width: "150px",
                ml: 2,
              }}
            >
              <Text
                variant="h6"
                sx={{
                  fontWeight: "600",
                  fontSize: "0.875rem",
                  fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                  color: "rgb(52, 71, 103)",
                }}
              >
                Peterson
              </Text>
              <Text sx={{ fontSize: 14, color: "rgb(123, 128, 154)" }}>
                Have a great afternoon..
              </Text>
            </Stack>
            <Stack>
              <Link
                sx={{
                  textDecoration: "none",
                  fontWeight: "bold",
                  textTransform: "uppercase",
                }}
              >
                Reply
              </Link>
            </Stack>
          </Stack>
        </Stack>
      </Box>
    </Stack>
  );
}

export default Conversations;
