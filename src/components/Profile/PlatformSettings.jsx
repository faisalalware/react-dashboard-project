import Box from "@mui/material/Box";
import { Stack, Typography } from "@mui/material";
import Switch from "@mui/material/Switch";
import { useState } from "react";
import Divider from "@mui/material/Divider";

function PlatformSettings() {
  const label = { inputProps: { "aria-label": "Switch demo" } };
  const [followsMe, setFollowsMe] = useState(true);
  const [answersPost, setAnswersPost] = useState(false);
  const [mentionsMe, setMentionsMe] = useState(true);
  const [newLaunches, setNewLaunches] = useState(false);
  const [productUpdate, setProductUpdate] = useState(true);
  const [newsletter, setNewsletter] = useState(false);

  return (
    <Stack divider={<Divider orientation="vertical" flexItem />} spacing={1} marginTop="2px">
      <Box p={3}>
        <Typography
          variant={"h6"}
          textTransform="capitalize"
          sx={{
            fontWeight: "700",
            fontSize: "1rem",
            fontFamily: "Roboto, Helvetica, Arial, sans-serif",
            color: "rgb(52, 71, 103)",
          }}
        >
          platform settings
        </Typography>
      </Box>

      <Box ml={3} pb={1} px={1} lineHeight={1}>
        <Typography
          variant="h6"
          textTransform="uppercase"
          sx={{
            fontFamily: "Roboto, Helvetica, Arial, sans-serif",
            fontSize: "0.75rem",
            color: "rgb(123, 128, 154)",
            fontWeight: " 700",
          }}
        >
          account
        </Typography>
        <Box display="flex" alignItems="center" mb={0.5} ml={-1.5}>
          <Box mt={0.5}>
            <Switch
              {...label}
              defaultChecked
              color="default"
              checked={followsMe}
              onChange={() => setFollowsMe(!followsMe)}
            />
          </Box>
          <Box width="80%" ml={0.5}>
            <Typography
              variant="h6"
              sx={{
                fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                fontSize: "0.875rem",
                color: "rgb(123, 128, 154)",
                fontWeight: "400",
              }}
            >
              Email me when someone follows <br></br>me
            </Typography>
          </Box>
        </Box>
        <Box display="flex" alignItems="center" mb={0.5} ml={-1.5}>
          <Box mt={0.5}>
            <Switch
              {...label}
              checked={answersPost}
              onChange={() => setAnswersPost(!answersPost)}
            />
          </Box>
          <Box width="80%" ml={0.5}>
            <Typography
              variant="h6"
              sx={{
                fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                fontSize: "0.875rem",
                color: "rgb(123, 128, 154)",
                fontWeight: "400",
              }}
            >
              Email me when someone answers <br></br>on my post
            </Typography>
          </Box>
        </Box>
        <Box display="flex" alignItems="center" mb={0.5} ml={-1.5}>
          <Box mt={0.5}>
            <Switch
              {...label}
              defaultChecked
              color="default"
              checked={mentionsMe}
              onChange={() => setMentionsMe(!mentionsMe)}
            />
          </Box>
          <Box width="80%" ml={0.5}>
            <Typography
              variant="h6"
              sx={{
                fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                fontSize: "0.875rem",
                color: "rgb(123, 128, 154)",
                fontWeight: "400",
              }}
            >
              Email me when someone <br></br>mentions me
            </Typography>
          </Box>
        </Box>
        <Box mt={3}>
          <Typography
            variant="h6"
            textTransform="uppercase"
            sx={{
              fontFamily: "Roboto, Helvetica, Arial, sans-serif",
              fontSize: "0.80rem",
              color: "rgb(123, 128, 154)",
              fontWeight: " 700",
            }}
          >
            application
          </Typography>
        </Box>
        <Box display="flex" alignItems="center" mb={0.5} ml={-1.5}>
          <Box mt={0.5}>
            <Switch
              {...label}
              checked={newLaunches}
              onChange={() => setNewLaunches(!newLaunches)}
            />
          </Box>
          <Box width="80%" ml={0.5}>
            <Typography
              variant="h6"
              sx={{
                fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                fontSize: "0.875rem",
                color: "rgb(123, 128, 154)",
                fontWeight: "400",
              }}
            >
              New launches and projects
            </Typography>
          </Box>
        </Box>
        <Box display="flex" alignItems="center" mb={0.5} ml={-1.5}>
          <Box mt={0.5}>
            <Switch
              {...label}
              defaultChecked
              color="default"
              checked={productUpdate}
              onChange={() => setProductUpdate(!productUpdate)}
            />
          </Box>
          <Box width="80%" ml={0.5}>
            <Typography
              variant="h6"
              sx={{
                fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                fontSize: "0.875rem",
                color: "rgb(123, 128, 154)",
                fontWeight: "400",
              }}
            >
              Monthly product updates
            </Typography>
          </Box>
        </Box>
        <Box display="flex" alignItems="center" mb={0.5} ml={-1.5}>
          <Box mt={0.5}>
            <Switch
              {...label}
              checked={newsletter}
              onChange={() => setNewsletter(!newsletter)}
            />
          </Box>
          <Box width="80%" ml={0.5}>
            <Typography
              variant="h6"
              sx={{
                fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                fontSize: "0.875rem",
                color: "rgb(123, 128, 154)",
                fontWeight: "400",
              }}
            >
              Subscribe to newsletter
            </Typography>
          </Box>
        </Box>
      </Box>
    </Stack>
  );
}

export default PlatformSettings;
