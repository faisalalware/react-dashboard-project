import { Stack, Typography, Box, Grid } from "@mui/material";
import ProjectCard from "./ProjectCard";

function Projects() {
  const projectData = [
    {
      src: "/Images/Profile/home-decor-1.jpg",
      project: "Project #2",
      title: "Modern",
      data: "As Uber works through a huge amount of internal management turmoil.",
      button:"View Project"
    },
    {
      src: "/Images/Profile/home-decor-2.jpg",
      project: "Project #1",
      title: "Scandinavian",
      data: "Music is something that everyone has their own specific opinion about.",
      button:"View Project"
    },
    {
      src: "/Images/Profile/home-decor-3.jpg",
      project: "Project #3",
      title: "Minimalist",
      data: "Different people have different taste, and various types of music.",
      button:"View Project"
    },
    {
      src: "/Images/Profile/home-decor-4.jpeg",
      project: "Project #4",
      title: "Gothic",
      data: "Why would anyone pick blue over pink? Pink is obviously a better color.",
      button:"View Project"
    },
  ];
  return (
    <>
      {/* <Box pt={2} px={2} lineHeight={1.25}> */}
      <Stack marginTop={"35px"}>
        <Typography
          variant={"h6"}
          sx={{
            fontWeight: "700",
            fontSize: "1rem",
            fontFamily: "Roboto, Helvetica, Arial, sans-serif",
            color: "rgb(52, 71, 103)",
          }}
        >
          Projects
        </Typography>
        {/* <Box mb={1}> */}
        {/* <Stack sx={{mb:"2" ,marginLeft:"-400px"}}> */}
        <Typography
          variant="h6"
          sx={{
            fontFamily: "Roboto, Helvetica, Arial, sans-serif",
            fontSize: "0.875rem",
            color: "rgb(123, 128, 154)",
            fontWeight: "400",
          }}
        >
          Architects design houses
        </Typography>
        {/* </Stack> */}
      {/* </Stack>
      <Stack> */}
        <Grid container spacing={2}>
          {projectData.map((d) => (
            <Grid item md={3} xs={12}>
              <ProjectCard
                src={d.src}
                project={d.project}
                title={d.title}
                data={d.data}
                button={d.button}
              />
            </Grid>
          ))}
        </Grid>
      </Stack>
    </>
  );
}
export default Projects;
