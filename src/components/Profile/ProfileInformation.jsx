import React from "react";
import { Stack, Typography, Box } from "@mui/material";
import Divider from "@mui/material/Divider";
import FacebookIcon from "@mui/icons-material/Facebook";
import TwitterIcon from "@mui/icons-material/Twitter";
import InstagramIcon from "@mui/icons-material/Instagram";

function ProfileInformation() {
  return (
    // <Stack
    //   divider={<Divider orientation="vertical" sx={{ ml: -2, mr: 1 }} />}
    //   spacing={2}
    // >

    <Stack divider={<Divider orientation="vertical" flexItem />} spacing={1}>
      <Box p={3.3}>
        <Typography
          variant={"h6"}
          textTransform="capitalize"
          sx={{
            fontWeight: "700",
            fontSize: "1rem",
            fontFamily: "Roboto, Helvetica, Arial, sans-serif",
            color: "rgb(52, 71, 103)",
          }}
        >
          Profile Information
        </Typography>
      </Box>

      <Box ml={4} pb={1} px={1} lineHeight={1}>
        <Typography
          variant="h6"
          sx={{
            fontFamily: "Roboto, Helvetica, Arial, sans-serif",
            fontSize: "0.875rem",
            color: "rgb(123, 128, 154)",
            fontWeight: "400",
          }}
        >
          Hi, I’m Alec Thompson, Decisions: If you can’t decide, the answer is
          no. If two equally difficult paths, choose the one more painful in the
          short term (pain avoidance is creating an illusion of equality).
        </Typography>
      </Box>
      <Box display="flex" alignItems="center" mb={0.5} ml={-1.5}>
        <Box mt={0.5}>
          <Typography
            variant="h6"
            sx={{
              fontFamily: "Roboto, Helvetica, Arial, sans-serif",
              fontSize: "0.875rem",
              textTransform: "capitalize",
              color: "rgb(52, 71, 103)",
              fontWeight: "700",
            }}
          >
            FullName:
          </Typography>
        </Box>
        <Box width="80%" ml={0.5}>
          <Typography
            variant="h6"
            sx={{
              fontFamily: "Roboto, Helvetica, Arial, sans-serif",
              fontSize: "0.875rem",
              color: "rgb(123, 128, 154)",
              fontWeight: "400",
            }}
          >
            Alec M. Thompson
          </Typography>
        </Box>
      </Box>
      <Box display="flex" alignItems="center" mb={0.5} ml={-1.5}>
        <Box mt={0.5}>
          <Typography
            variant="h6"
            sx={{
              fontFamily: "Roboto, Helvetica, Arial, sans-serif",
              fontSize: "0.875rem",
              textTransform: "capitalize",
              color: "rgb(52, 71, 103)",
              fontWeight: "700",
            }}
          >
            Mobile:
          </Typography>
        </Box>
        <Box width="80%" ml={0.5}>
          <Typography
            variant="h6"
            sx={{
              fontFamily: "Roboto, Helvetica, Arial, sans-serif",
              fontSize: "0.875rem",
              color: "rgb(123, 128, 154)",
              fontWeight: "400",
            }}
          >
            (44) 123 1234 123
          </Typography>
        </Box>
      </Box>
      <Box display="flex" alignItems="center" mb={0.5} ml={-1.5}>
        <Box mt={0.5}>
          <Typography
            variant="h6"
            sx={{
              fontFamily: "Roboto, Helvetica, Arial, sans-serif",
              fontSize: "0.875rem",
              textTransform: "capitalize",
              color: "rgb(52, 71, 103)",
              fontWeight: "700",
            }}
          >
            Email:
          </Typography>
        </Box>
        <Box width="80%" ml={0.5}>
          <Typography
            variant="h6"
            sx={{
              fontFamily: "Roboto, Helvetica, Arial, sans-serif",
              fontSize: "0.875rem",
              color: "rgb(123, 128, 154)",
              fontWeight: "400",
            }}
          >
            alecthompson@mail.com
          </Typography>
        </Box>
      </Box>
      <Box display="flex" alignItems="center" mb={0.5} ml={-1.5}>
        <Box mt={0.5}>
          <Typography
            variant="h6"
            sx={{
              fontFamily: "Roboto, Helvetica, Arial, sans-serif",
              fontSize: "0.875rem",
              textTransform: "capitalize",
              color: "rgb(52, 71, 103)",
              fontWeight: "700",
            }}
          >
            Location:
          </Typography>
        </Box>
        <Box width="80%" ml={0.5}>
          <Typography
            variant="h6"
            sx={{
              fontFamily: "Roboto, Helvetica, Arial, sans-serif",
              fontSize: "0.875rem",
              color: "rgb(123, 128, 154)",
              fontWeight: "400",
            }}
          >
            USA
          </Typography>
        </Box>
      </Box>

      <Box display="flex" alignItems="center" mb={0.5} ml={-1.5}>
        <Box mt={0.5}>
          <Typography
            variant="h6"
            sx={{
              fontFamily: "Roboto, Helvetica, Arial, sans-serif",
              fontSize: "0.875rem",
              textTransform: "capitalize",
              color: "rgb(52, 71, 103)",
              fontWeight: "700",
            }}
          >
            Social:
          </Typography>
        </Box>
        <Box width="80%" ml={0.5}>
          <a href="https://www.facebook.com/CreativeTim/">
            <FacebookIcon sx={{color:"rgb(59, 89, 152)"}} />
          </a>

          <a href="https://twitter.com/creativetim">
            <TwitterIcon  sx={{color:"rgb(85, 172, 238)"}}/>
          </a>

          <a href="https://www.instagram.com/creativetimofficial/">
            <InstagramIcon sx={{color:"rgb(18, 86, 136)"}} />
          </a>
        </Box>
      </Box>
    </Stack>
  );
}

export default ProfileInformation;
