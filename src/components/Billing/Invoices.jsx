import React from 'react'

import{ Grid, Paper, Typography, TableContainer, Table, TableRow, TableCell, TableBody} from '@mui/material';

import PictureAsPdfIcon from '@mui/icons-material/PictureAsPdf';

const table = [{
    date: "March, 01, 2020",
    id: "#MS-415646",
    price: "$180",
    icon: ''
  },
  {
    date: "March, 01, 2020",
    id: "#MS-415646",
    price: "$180",
    icon: ''
  },
  {
    date: "March, 01, 2020",
    id: "#MS-415646",
    price: "$180",
    icon: ''
  },
  {
    date: "March, 01, 2020",
    id: "#MS-415646",
    price: "$180",
    icon: ''
  },
  {
    date: "March, 01, 2020",
    id: "#MS-415646",
    price: "$180",
    icon: ''
  },
  ]


function Invoices() {
  return (
    <Grid item lg={4} xs={8}>

            <Paper sx={{
              borderRadius: '12px',
              pb: '25px'
            }} elevation={5}>
              <Typography sx={{
                // textAlign: 'center',
                padding: '15px',
                fontWeight: '800',
                fontSize: '14px'
              }}>Invoices</Typography>
              <TableContainer sx={{
                marginBottom: '5px',
                borderRadius: '12px',
              }}>
                <Table
                  sx={{
                    background: "rgb(255, 255, 255)",
                    color: "rgb(123, 128, 154)",
                    opacity: "0.7",
                    fontFamily: "Roboto, Helvetica, Arial, sans-serif",

                  }}
                  aria-label="simple table"
                >
                  <TableBody>
                    {table.map((row) => (
                      <TableRow>
                        <TableCell>
                          <Grid container>
                            <Grid item lg={12}>
                              <Typography sx={{ fontWeight: "800", color: "black", fontSize: '13px' }}>
                                {row.date}
                              </Typography>
                            </Grid>
                            <Grid item xs={12}>
                              <Typography sx={{ fontSize: '10px' }}>
                                {row.id}
                              </Typography>
                            </Grid>
                          </Grid>
                        </TableCell>
                        <TableCell
                          sx={{ fontSize: "14px", color: "rgb(52, 71, 103)", opacity: 1 }}
                        >
                          {row.price}
                        </TableCell>
                        <TableCell
                          sx={{ fontWeight: "bold", fontSize: "12px", }}
                        >
                          <PictureAsPdfIcon />
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Paper>

          </Grid>
  )
}

export default Invoices