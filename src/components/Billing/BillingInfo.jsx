import React from 'react'
import { Stack, Grid, Paper, Typography } from '@mui/material'

import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';

const billingInfo = [{
    name: 'Oliver Liam',
    company: " Viking Burrito",
    email: " oliver@burrito.com",
    vatNumber: "FRB1235476"
}, {
    name: "Lucas Harper",
    company: "Stone Tech Zone",
    email: "lucas@stone-tech.com",
    vatNumber: "FRB1235476"
}, {
    name: "Ethan James",
    company: "Fiber Notion",
    email: "ethan@fiber.com",
    vatNumber: "FRB1235476"
}
];


function BillingInfo() {
    return (
        <Grid item md={7} xs={8}  >
            <Paper elevation={5} sx={{ borderRadius: '15px', padding: '15px 25px', }}>
                <Typography sx={{
                    padding: '15px 25px',
                    fontWeight: '800',
                    fontSize: '14px'
                }}>
                    Billing Information
                </Typography>
                {billingInfo.map((row) => (
                    <Stack sx={{ backgroundColor: 'rgb(247, 250, 255)', margin: '15px 10px', }}>
                        <Grid container sx={{ padding: '15px 25px', }}>
                            <Grid item lg={8} xs={6} >
                                <Typography sx={{ fontWeight: '800', }}>{row.name}</Typography>
                            </Grid>
                            <Grid item lg={4} xs={6} sx={{ display: 'flex', justifyContent: 'space-between' }}>
                                <DeleteIcon color={"error"}/>Delete
                                <EditIcon />Edit
                            </Grid>
                        </Grid>
                        <Grid sx={{ padding: '15px 25px' }}>
                            <Typography variant={"subtitle2"}>Company Name: <b>{row.company}</b></Typography>
                            <Typography variant={"subtitle2"}>Email Address: <b>{row.email}</b></Typography>
                            <Typography variant={"subtitle2"}>VAT Number: <b>{row.vatNumber}</b></Typography>
                        </Grid>
                    </Stack>
                ))}
            </Paper>
        </Grid>
    )
}

export default BillingInfo