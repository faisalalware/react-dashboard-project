import React from 'react'

import {Grid, Paper, Typography } from "@mui/material"

import ArrowDropDownCircleOutlinedIcon from '@mui/icons-material/ArrowDropDownCircleOutlined';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';


const newest = [{
    // icon: <ArrowDropDownCircleOutlinedIcon/>,
    name: "Netflix",
    date: "27 March 2020, at 12:30 PM",
    price: "- $1,500"
  }, {
    name: "Apple",
    date: "27 March 2020, at 04:30 AM",
    price: "- $2,000"
  }]

  const yesterday = [{
    name: "Stripe",
    date: "27 March 2020, at 04:30 AM",
    price: "+ $750"
  },
  {
    name: "Apple",
    date: "27 March 2020, at 04:30 AM",
    price: "+ $2,000"
  },
  {
    name: "HubSpot",
    date: "27 March 2020, at 04:30 AM",
    price: "+ $1,000"
  },
  {
    name: "Webflow",
    date: "27 March 2020, at 04:30 AM",
    price: "+ $2,500"
  }
  ]

function Transactions() {
  return (
    <Grid item md={5} xs={8}>
    <Paper elevation={5} sx={{
      borderRadius: "15px",
      padding: '15px 25px',
      marginBottom: '15px'
    }}>
      <Grid sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <Typography sx={{ fontWeight: '800', fontSize: '14px' }}>Your Transaction's</Typography>
        <Grid  >
          <CalendarMonthIcon />
          <Typography variant={"subtutle2"}>23 - 30 March 2020</Typography>
        </Grid>
      </Grid>
      <Grid sx={{ padding: '15px 0' }}>
        <Grid>
          <Typography>Newest</Typography>
          {newest.map((row) => (
            <Grid container sx={{ padding: '15px 15px' }}>
              <Grid item lg={2} xs={2}>
                {/* <Avatar src= "<ArrowDropDownCircleOutlinedIcon/>" /> */}
                <ArrowDropDownCircleOutlinedIcon />
              </Grid>
              <Grid item lg={8} xs={8}>
                <Typography sx={{ fontWeight: "700", color: "black" }}>
                  {row.name}
                </Typography>
                <Typography variant="subtitle2">{row.date}</Typography>
              </Grid>
              <Grid item lg={2} sx={{ fontSize: '14px', color: 'red'}}>{row.price}</Grid>
            </Grid>
          ))}
        </Grid>
        <Grid>
          <Typography>Yesterday</Typography>
          {yesterday.map((row) => (
            <Grid container sx={{ padding: '15px 15px' }}>
              <Grid item lg={2} xs={2}>
                <ArrowDropDownCircleOutlinedIcon color={"success"} />
              </Grid>
              <Grid item lg={8} xs={8}>
                <Typography sx={{ fontWeight: "700", color: "black" }}>
                  {row.name}
                </Typography>
                <Typography variant="subtitle2">{row.date}</Typography>
              </Grid>
              <Grid item lg={2} sx={{ fontSize: '14px', color: "#4caf50" }}>{row.price}</Grid>
            </Grid>
          ))}
        </Grid>
      </Grid>

    </Paper>
  </Grid>
  )
}

export default Transactions