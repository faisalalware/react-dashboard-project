import React from 'react'
import { Stack, Grid, Box } from '@mui/material'

import BillingCards from './BillingCards';
import Payments from './Payment';
import Invoices from './Invoices';
import BillingInfo from './BillingInfo';
import Transactions from './Transactions';

function Billing() {

  return (
    <Stack sx={{ marginTop: '25px' }}>
      <Box sx={{ flexGrow: 1 }} >
        <Grid container spacing={2}>
          <BillingCards />
          <Invoices />
        </Grid>
      </Box>
      <Payments />
      <Grid container spacing={2} sx={{ mt: '5px', }}>
        <BillingInfo />
        <Transactions />
      </Grid>
    </Stack>
  )
}

export default Billing