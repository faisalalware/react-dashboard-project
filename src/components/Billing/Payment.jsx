import React from 'react'

import {Box, Grid, Paper, Typography, Button} from '@mui/material'

function Payment() {
  return (
    <Box sx={{ flexGrow: 1, mt: { lg: "-175px", xs: '30px' } }}>
        <Grid container spacing={2}>
          <Grid item md={8} xs={8}>
            <Paper elevation={5} sx={{ borderRadius: "15px", padding: '15px', pb: '0px' }} >
              <Grid sx={{
                display: 'flex',
                justifyContent: 'space-between',

              }}>
                <Typography sx={{
                  padding: '15px 15px',
                  fontWeight: '800',
                  fontSize: '14px'
                }}>Payment Method
                </Typography>
                <Button sx={{
                  marginRight: '35px',
                  padding: '6px 25px',
                  backgroundColor: 'black',
                  color: 'white',
                  fontSize: '11px',
                  fontWeight: '800',
                  borderRadius: '9px'
                }}>+Add new card
                </Button>
              </Grid>

              <Grid xs={6} sx={{
                display: 'flex',
                justifyContent: 'space-between',
                padding: '10px 20px'
              }}>
                <img src="/Images/Billing/mastercard.png" alt="" style={{ margin: '10px 0', width: '290px', border: '1px solid rgb(52, 71, 103)', borderRadius: '15px' }} />
                <img src="/Images/Billing/visa.png" alt="" style={{ margin: '10px', width: '300px', border: '1px solid rgb(52, 71, 103)', borderRadius: '15px' }} />
              </Grid>
            </Paper>
          </Grid>
        </Grid>
      </Box>
  )
}

export default Payment