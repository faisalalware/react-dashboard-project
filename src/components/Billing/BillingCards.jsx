import React from 'react'

import { Grid, Paper, Typography, Divider } from '@mui/material'

function BillingCards() {
    return (
        <>
            <Grid item md={4} xs={10}>
                <img src="/Images/Billing/card1.png" alt="" />
            </Grid>
            <Grid item md={2} xs={8}>
                <Paper sx={{
                    borderRadius: '13px',
                    backgroundColor: 'rgb(255, 255, 255)'
                }} elevation={4}>
                    <img src='/Images/Billing/bank.png' alt="" style={{
                        padding: '20px 45px',
                    }} />
                    <Typography sx={{
                        textAlign: 'center',
                        fontWeight: '800',
                        fontSize: '14px'
                    }}>
                        Salary
                    </Typography>
                    <Typography sx={{
                        textAlign: 'center',
                        padding: '5px ', pb: '15px',
                        // fontWeight: '800',
                        fontSize: '12px'
                    }}>
                        Belong Interactive
                    </Typography>
                    <Divider variant='middle' />
                    <Typography sx={{
                        textAlign: 'center',
                        padding: '18px ',
                        fontWeight: '800',
                        fontSize: '16px'
                    }}>
                        +$2000
                    </Typography>
                </Paper>
            </Grid>
            <Grid item lg={2} xs={8}>
                <Paper sx={{
                    borderRadius: '13px',
                    backgroundColor: 'rgb(255, 255, 255)'
                }} elevation={4}>
                    <img src='/Images/Billing/paypal.png' alt="" style={{
                        padding: '20px 45px',
                    }} />
                    <Typography sx={{
                        textAlign: 'center',
                        fontWeight: '800',
                        fontSize: '14px'
                    }}>
                        Paypal
                    </Typography>
                    <Typography sx={{
                        textAlign: 'center',
                        padding: '5px ', pb: '15px',
                        // fontWeight: '800',
                        fontSize: '12px'
                    }}>
                        Freelance Payment
                    </Typography>
                    <Divider variant='middle' />
                    <Typography sx={{
                        textAlign: 'center',
                        padding: '18px ',
                        fontWeight: '800',
                        fontSize: '16px'
                    }}>
                        $455.00
                    </Typography>
                </Paper>
            </Grid>
        </>

    )
}

export default BillingCards