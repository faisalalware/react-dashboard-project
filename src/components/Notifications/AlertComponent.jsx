import { useState } from "react";
import Collapse from "@mui/material/Collapse";
import Alert from "@mui/material/Alert";
import IconButton from "@mui/material/IconButton";

import CloseIcon from "@mui/icons-material/Close";

const AlertComponent = (props) => {
  const { variant, severity } = props;
  const [open, setOpen] = useState(true);
  
  return (
    <Collapse in={open}>
      <Alert
        variant={variant}
        severity={severity}
        action={
          <IconButton
            color="inherit"
            size="small"
            onClick={() => {
              setOpen(false);
            }}
          >
            <CloseIcon fontSize="inherit" />
          </IconButton>
        }
        sx={{
          my: 1,
          fontSize: "1rem",
          fontWeight: "300",
          lineHeight: "1.6",
          letterSpacing: "0.01071em",
        }}
      >
        A simple error alert with an example link. Give it a click if you like.
      </Alert>
    </Collapse>
  );
};

export default AlertComponent;
