// import JarvisCard from '../common/JarvisCard'
// import Text from '../common/Text'
import React from "react";
import Stack from "@mui/material/Stack";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import { Typography } from "@mui/material";
import Button from "@mui/material/Button";
import AlertComponent from "./AlertComponent";

function Notifications() {
  return (
    // <Stack>
    //   <JarvisCard>
    //     <Text variant="small">Notifications</Text>
    //   </JarvisCard>
    // </Stack>
    <Box mt={6} mb={3}>
      <Grid container spacing={10} justifyContent="center">
        <Grid item xs={12} lg={8}>
          <Card>
            <Box p={2}>
              <Typography
                variant="h5"
                sx={{
                  color: "rgb(52, 71, 103)",
                  fontWeight: "700",
                  fontSize: "1.25rem",
                  fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                }}
              >
                Alerts
              </Typography>
            </Box>
            <Box pt={2} px={2}>
              <Stack sx={{ width: "100%" }} spacing={1}>
                <AlertComponent variant="filled" severity="success" />
                <AlertComponent variant="filled" severity="error" />
                <AlertComponent variant="filled" severity="warning" />
                <AlertComponent variant="filled" severity="info" />
              </Stack>
            </Box>
          </Card>
        </Grid>
        <Grid item xs={12} lg={8}>
          <Card>
            <Box p={1} lineHeight={0}>
              <Typography
                variant="h5"
                sx={{
                  color: "rgb(52, 71, 103)",
                  fontWeight: "700",
                  fontSize: "1.25rem",
                  fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                }}
              >
                Notifications
              </Typography>
              <Typography
                sx={{
                  color: "#7B809A",
                  fontSize: "14px",
                  fontWeight: "300",
                  fontFamily: " Roboto, Helvetica, Arial, sans-serif",
                }}
              >
                Notifications on this page use Toasts from Bootstrap. Read more
                details here.
              </Typography>
            </Box>
            <Box p={2}>
              <Grid container spacing={3}>
                <Grid item xs={12} sm={6} lg={3}>
                  <Button
                    sx={{
                      background:
                        "linear-gradient(195deg, rgb(102, 187, 106), rgb(67, 160, 71))",
                      color: "white",
                      fontFamily: " Roboto, Helvetica, Arial, sans-serif",
                      fontWeight: "700",
                      borderRadius: "0.5rem",
                    }}
                    // onClick={openSuccess}
                    fullWidth
                  >
                    success notification
                  </Button>
                  {/* {renderSuccess} */}
                </Grid>
                <Grid item xs={12} sm={6} lg={3}>
                  <Button
                    sx={{
                      background:
                        "linear-gradient(195deg, rgb(73, 163, 241), rgb(26, 115, 232))",
                      color: "white",
                      fontFamily: " Roboto, Helvetica, Arial, sans-serif",
                      fontWeight: "700",
                      borderRadius: "0.5rem",
                    }}
                    // onClick={openInfo}
                    fullWidth
                  >
                    info notification
                  </Button>
                  {/* {renderInfo} */}
                </Grid>
                <Grid item xs={12} sm={6} lg={3}>
                  <Button
                    sx={{
                      background:
                        "linear-gradient(195deg, rgb(255, 167, 38), rgb(251, 140, 0))",
                      color: "white",
                      fontFamily: " Roboto, Helvetica, Arial, sans-serif",
                      fontWeight: "700",
                      borderRadius: "0.5rem",
                    }}
                    // onClick={openWarning}
                    fullWidth
                  >
                    warning notification
                  </Button>
                  {/* {renderWarning} */}
                </Grid>
                <Grid item xs={12} sm={6} lg={3}>
                  <Button
                    sx={{
                      background:
                        "linear-gradient(195deg, rgb(239, 83, 80), rgb(229, 57, 53))",
                      color: "white",
                      fontFamily: " Roboto, Helvetica, Arial, sans-serif",
                      fontWeight: "700",
                      borderRadius: "0.5rem",
                    }}
                    // onClick={openError}
                    fullWidth
                  >
                    error notification
                  </Button>
                  {/* {renderError} */}
                </Grid>
              </Grid>
            </Box>
          </Card>
        </Grid>
      </Grid>
    </Box>
  );
}

export default Notifications;
