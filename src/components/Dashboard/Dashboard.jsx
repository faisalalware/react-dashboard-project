import React from "react";
import { Stack } from "@mui/material";
import DashboardCard from "../common/DashboardCard";
// import Text from '../common/Text'
import StatusCard from "./StatusCard";
import Grid from "@mui/material/Grid";
import ChartCard from "./ChartCard";
import Projects from "./Projects";
import OrderOverviews from "./OrderOverviews";

function Dashboard() {
  const statusData = [
    {
      src: "/images/Dashboard/couch-icon.png",
      type: "Bookings",
      data: "281",
      update: "+55%",
      status: "than last week",
    },
    {
      src: "/images/Dashboard/bar-icon.png",
      type: "Today's Users",
      data: "2,300",
      update: "+3%",
      status: "than last month",
    },
    {
      src: "/images/Dashboard/house-icon.png",
      type: "Revenue",
      data: "34k",
      update: "+1%",
      status: "than yesterday",
    },
    {
      src: "/images/Dashboard/user-icon.png",
      type: "Followers",
      data: "+91",
      status: "Just updated",
    },
  ];

  const chartData = [
    {
      src: "/images/Dashboard/blue-chart.png",
      type: "Website Views",
      data: "Last Campaign Performance",
      status: "campaign sent 2 days ago",
    },
    {
      src: "/images/Dashboard/green-chart.png",
      type: "Daily Sales",
      data: "(+15%) increase in today sales",
      status: "updated 4 min ago",
    },
    {
      src: "/images/Dashboard/black-chart.png",
      type: "Completed Tasks",
      data: "Last Campaign Performance",
      status: "just updated",
    },
  ];

  return (
    <>
      <Stack
        sx={{
          mt: 5,
          mb: 5,
        }}
      >
        <Grid container spacing={3}>
          {statusData.map((d) => (
            <Grid item md={3} xs={12}>
              <StatusCard
                src={d.src}
                type={d.type}
                data={d.data}
                update={d.update}
                status={d.status}
              />
            </Grid>
          ))}
        </Grid>
      </Stack>

      <Stack
        sx={{
          mt: 5,
          mb: 5,
        }}
      >
        <Grid container spacing={3}>
          {chartData.map((d) => (
            <Grid item md={4} xs={12}>
              <ChartCard
                src={d.src}
                type={d.type}
                data={d.data}
                status={d.status}
              />
            </Grid>
          ))}
        </Grid>
      </Stack>

      <Stack
        sx={{
          mt: 2,
          mb: 5,
        }}
      >
        <Grid container spacing={3}>
          <Grid item md={8} xs={12}>
            <Projects />
          </Grid>
          <Grid item md={4} sx={12}>
            <OrderOverviews />
          </Grid>
        </Grid>
      </Stack>
      
      {/* <OrderOverviews/> */}
    </>
  );
}

export default Dashboard;
