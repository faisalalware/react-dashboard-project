import React from "react";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import DoneIcon from "@mui/icons-material/Done";
import DashboardCard from "../common/DashboardCard";

import {
  // Card,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  // Paper,
  Avatar,
  Grid,
  Typography,
  AvatarGroup,
} from "@mui/material";

const table = [
  {
    src: "/Images/Dashboard/pro1.png",
    name: "Material UI XD Version",
    members: "/Images/Tables/img1.png",
    budget: "$14000",
    completion: "images/Dashboard/comp1.png",
  },
  {
    src: "/Images/Dashboard/pro2.png",
    name: "Add Progress Track",
    members: "/Images/Tables/img2.png",
    budget: "$14000",
    completion: "images/Dashboard/comp2.png",
  },
  {
    src: "/Images/Dashboard/pro3.png",
    name: "Fix Platform Errors",
    members: "/Images/Tables/img2.png",
    budget: "$14000",
    completion: "images/Dashboard/comp3.png",
  },
  {
    src: "/Images/Dashboard/pro4.png",
    name: "Launch our Mobile App",
    members: "/Images/Tables/img2.png",
    budget: "$14000",
    completion: "images/Dashboard/comp3.png",
  },
  {
    src: "/Images/Dashboard/pro5.png",
    name: "Add the New Pricing Page",
    members: "/Images/Tables/img2.png",
    budget: "$14000",
    completion: "images/Dashboard/comp5.png",
  },
  {
    src: "/Images/Dashboard/pro6.png",
    name: "Redesign New Online Shop",
    members: "/Images/Tables/img2.png",
    budget: "$14000",
    completion: "images/Dashboard/comp6.png",
  },
];

function Projects() {
  return (
    <>
      <DashboardCard>
        <Grid container>
          <Grid item lg={11}>
            <Typography
              variant={"h6"}
              sx={{
                fontWeight: "700",
                fontSize: "1rem",
                fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                color: "rgb(52, 71, 103)",
              }}
            >
              Projects
            </Typography>
            <Grid>
              <DoneIcon color="primary" fontSize="medium" />
              <Typography
                variant={"h7"}
                sx={{ color: "rgb(52, 71, 103)", opacity: "0.7" }}
              >
                <b>30 done</b> this month
              </Typography>
            </Grid>
          </Grid>
          <Grid item lg={1} sx={{ color: "rgb(52, 71, 103)", opacity: "0.7" }}>
            <MoreVertIcon />
          </Grid>
        </Grid>

        <TableContainer sx={{ marginBottom: "-80px", mt: "35px" }}>
          <Table
            sx={{
              background: "rgb(255, 255, 255)",
              color: "rgb(123, 128, 154)",
              opacity: "0.7",
              fontFamily: "Roboto, Helvetica, Arial, sans-serif",
              borderRadius: "20px",
            }}
            aria-label="simple table"
          >
            <TableHead>
              <TableRow
                sx={{
                  textTransform: "uppercase",
                  fontSize: "10px",
                }}
              >
                <TableCell
                  sx={{
                    fontSize: "10px",
                  }}
                >
                  Companies
                </TableCell>
                <TableCell sx={{ fontSize: "10px" }}>Members</TableCell>
                <TableCell sx={{ fontSize: "10px" }}>Budget</TableCell>
                <TableCell
                  sx={{
                    fontSize: "10px",
                    padding: "0 45px ",
                  }}
                >
                  Completion
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {table.map((row) => (
                <TableRow
                  key={row.name}
                  sx={{
                    "&:last-child td, &:last-child th": { border: 0 },
                  }}
                >
                  <TableCell>
                    <Grid container>
                      <Grid item lg={2}>
                        <Avatar
                          src={row.src}
                          sx={{ width: "25px", height: "25px" }}
                        />
                      </Grid>
                      <Grid item lg={10}>
                        <Typography
                          sx={{
                            fontWeight: "800",
                            color: "black",
                            fontSize: "12px",
                          }}
                        >
                          {row.name}
                        </Typography>
                      </Grid>
                    </Grid>
                  </TableCell>
                  <TableCell>
                    <AvatarGroup max={4}>
                      {/* <Avatar src='/Images/Tables/img1.png' /> */}
                      <Avatar
                        src="/Images/Tables/img2.png"
                        sx={{ width: "20px", height: "20px" }}
                      />
                      <Avatar
                        src="/Images/Tables/img1.png"
                        sx={{ width: "20px", height: "20px" }}
                      />
                      <Avatar
                        src="/Images/Tables/img2.png"
                        sx={{ width: "20px", height: "20px" }}
                      />
                    </AvatarGroup>
                    {/* <img src={} alt="" /> */}
                  </TableCell>
                  <TableCell sx={{ fontWeight: "800", fontSize: "12px" }}>
                    {row.budget}
                  </TableCell>
                  <TableCell>
                    <img src={row.completion} alt="" width="120px" />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </DashboardCard>
    </>
  );
}

export default Projects;
