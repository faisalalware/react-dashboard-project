import React from "react";
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import DashboardCard from "../common/DashboardCard";
import { Grid, Typography } from "@mui/material";

import { Timeline } from "@mui/lab";
import TimelineItem from "@mui/lab/TimelineItem";
import TimelineSeparator from "@mui/lab/TimelineSeparator";
import TimelineConnector from "@mui/lab/TimelineConnector";
import TimelineContent from "@mui/lab/TimelineContent";
import TimelineDot from "@mui/lab/TimelineDot";

import NotificationsIcon from "@mui/icons-material/Notifications";
import Inventory2Icon from "@mui/icons-material/Inventory2";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import PaymentIcon from "@mui/icons-material/Payment";
import VpnKeyIcon from "@mui/icons-material/VpnKey";
import Stack from "@mui/material/Stack";

function OrderOverviews() {
  return (
    <Stack >
      <DashboardCard>
        <Stack position={"relative"} sx={{display:'flex',flexDirection:'column'}}>
          <Typography
            variant={"h6"}
            sx={{
              fontWeight: "700",
              fontSize: "1rem",
              fontFamily: "Roboto, Helvetica, Arial, sans-serif",
              color: "rgb(52, 71, 103)",
              pb: '18px'
            }}
          >
            Orders overviews
          </Typography>
          <Grid>
            <ArrowUpwardIcon color="success" fontSize="medium" />
            <Typography
              variant={"h7"}
              sx={{ color: "rgb(52, 71, 103)", opacity: "0.7" }}
            >
              <b>24%</b> this month
            </Typography>
          </Grid>
        </Stack>

        <Stack ml={-30}>
          <Timeline  position={"absolute"} sx={{ left:"0"}}>
            <TimelineItem>
              <TimelineSeparator>
                <TimelineConnector />
                <TimelineDot color="success">
                  <NotificationsIcon />
                </TimelineDot>
                <TimelineConnector />
              </TimelineSeparator>
              <TimelineContent sx={{ py: "12px", px: 2 }}>
                <Typography
                  variant={"h6"}
                  sx={{
                    fontWeight: "700",
                    fontSize: "0.875rem",
                    fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                    color: "rgb(52, 71, 103)",
                  }}
                >
                  $2400, Design changes
                </Typography>
                <Typography
                  variant={"h6"}
                  sx={{
                    fontWeight: "300",
                    fontSize: " 0.75rem",
                    fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                    color: "rgb(123, 128, 154)",
                  }}
                >
                  22 DEC 7:20 PM
                </Typography>
              </TimelineContent>
            </TimelineItem>

            <TimelineItem>
              <TimelineSeparator>
                <TimelineConnector />
                <TimelineDot color="error">
                  <Inventory2Icon />
                </TimelineDot>
                <TimelineConnector />
              </TimelineSeparator>
              <TimelineContent sx={{ py: "12px", px: 2 }}>
                <Typography
                  variant={"h6"}
                  sx={{
                    fontWeight: "700",
                    fontSize: "0.875rem",
                    fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                    color: "rgb(52, 71, 103)",
                  }}
                >
                  New order #1832412
                </Typography>
                <Typography
                  variant={"h6"}
                  sx={{
                    fontWeight: "300",
                    fontSize: " 0.75rem",
                    fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                    color: "rgb(123, 128, 154)",
                  }}
                >
                  21 DEC 11 PM
                </Typography>
              </TimelineContent>
            </TimelineItem>

            <TimelineItem>
              <TimelineSeparator>
                <TimelineConnector />
                <TimelineDot color="info">
                  <ShoppingCartIcon />
                </TimelineDot>
                <TimelineConnector />
              </TimelineSeparator>
              <TimelineContent sx={{ py: "12px", px: 2 }}>
                <Typography
                  variant={"h6"}
                  sx={{
                    fontWeight: "700",
                    fontSize: "0.875rem",
                    fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                    color: "rgb(52, 71, 103)",
                  }}
                >
                  Server payments for April
                </Typography>
                <Typography
                  variant={"h6"}
                  sx={{
                    fontWeight: "300",
                    fontSize: " 0.75rem",
                    fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                    color: "rgb(123, 128, 154)",
                  }}
                >
                  21 DEC 9:34 PM
                </Typography>
              </TimelineContent>
            </TimelineItem>

            <TimelineItem>
              <TimelineSeparator>
                <TimelineConnector />
                <TimelineDot color="warning">
                  <PaymentIcon />
                </TimelineDot>
                <TimelineConnector />
              </TimelineSeparator>
              <TimelineContent sx={{ py: "12px", px: 2 }}>
                <Typography
                  variant={"h6"}
                  sx={{
                    fontWeight: "700",
                    fontSize: "0.875rem",
                    fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                    color: "rgb(52, 71, 103)",
                  }}
                >
                  New card added for order #4395133
                </Typography>
                <Typography
                  variant={"h6"}
                  sx={{
                    fontWeight: "300",
                    fontSize: " 0.75rem",
                    fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                    color: "rgb(123, 128, 154)",
                  }}
                >
                  20 DEC 2:20 AM
                </Typography>
              </TimelineContent>
            </TimelineItem>

            <TimelineItem>
              <TimelineSeparator>
                <TimelineConnector />
                <TimelineDot color="secondary">
                  <VpnKeyIcon />
                </TimelineDot>
                <TimelineConnector />
              </TimelineSeparator>
              <TimelineContent sx={{ py: "12px", px: 2 }}>
                <Typography
                  variant={"h6"}
                  sx={{
                    fontWeight: "700",
                    fontSize: "0.875rem",
                    fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                    color: "rgb(52, 71, 103)",
                  }}
                >
                  New card added for order #4395133
                </Typography>
                <Typography
                  variant={"h6"}
                  sx={{
                    fontWeight: "300",
                    fontSize: " 0.75rem",
                    fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                    color: "rgb(123, 128, 154)",
                  }}
                >
                  18 DEC 4:54 AM
                </Typography>
              </TimelineContent>
            </TimelineItem>
          </Timeline>
        </Stack>
      </DashboardCard>
    </Stack>
  );
}

export default OrderOverviews;
