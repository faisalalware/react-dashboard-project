import React from 'react'
import { Stack } from '@mui/material'
import DashboardCard from '../common/DashboardCard'
import Text from '../common/Text'
import Divider from '@mui/material/Divider'
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import { fontWeight } from '@mui/system'

const ChartCard = (props) => {
    const {src, type, data, status} = props;
  return (
    <Stack>
        <DashboardCard sx={{
            padding: "15px 0"
        }}>
            <Stack sx={{
                marginTop: "-40px",
            }}>
                <img src={src} alt="" style={{ maxWidth: "100%" }} />
            </Stack>
            <Stack sx={{
                mt:3,
                mb:2
            }}>
                <Text sx={{ color: "#344767", fontWeight: "bold", fontSize: 18 }}>{type}</Text>
                <Text sx={{ color: "#7b809a" }}>{data}</Text>
            </Stack>
            <Divider sx={{
                opacity: "0.3"
            }} />
            <Stack sx={{
                flexDirection: "row",
                mt: 2
            }}>
                <AccessTimeIcon sx={{ color: "7b809a" }} />
                <Text sx={{ color: "#7b809a", ml:1 }}>{status}</Text>
            </Stack>
        </DashboardCard>
    </Stack>
  )
}

export default ChartCard