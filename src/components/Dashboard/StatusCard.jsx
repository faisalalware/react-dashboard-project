import React from 'react'
import { Stack } from '@mui/material'
import DashboardCard from '../common/DashboardCard'
import Text from '../common/Text'
import Divider from '@mui/material/Divider'

const StatusCard = (props) => {
    const {src, type, data, update, status} = props;
  return (
    <Stack>
        <DashboardCard sx={{
            padding: "15px 0"
        }}>
            <Stack sx={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
                mb:2
            }}>
                <Stack sx={{
                    marginTop:"-50px",
                    marginLeft: "-12px"
                }}>
                <img src={src} alt="couch-icon"/>
                </Stack>
                <Stack sx={{
                    textAlign: "right"
                }}>
                    <Text variant="small" sx={{ color: "#7b809a" }}>{type}</Text>
                    <Text variant="small" sx={{ color: "#344767", fontWeight:"bold", fontSize: 25 }}>{data}</Text>
                </Stack>
            </Stack>
            <Divider sx={{ opacity: "0.3" }} />
            <Stack sx={{
                mt:2
            }}>
                <Text sx={{ color: "#7b809a" }}><span style={{ color: "#4caf50", fontWeight: "bold" }}>{update} </span>{status}</Text>
            </Stack>
        </DashboardCard>
    </Stack>
  )
}

export default StatusCard