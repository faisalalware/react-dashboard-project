import React from 'react'
import { Stack } from '@mui/material'
import DashboardCard from '../common/DashboardCard'
import Text from '../common/Text'

function RTL() {
  return (
    <Stack>
    <DashboardCard>
      <Text variant="small">RTL</Text>
    </DashboardCard>
  </Stack>
  )
}

export default RTL