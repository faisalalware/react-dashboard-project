import { Card, Paper } from '@mui/material'
import React from 'react'

const CardHeader = (props) => {

    // const title= props;

  return (
    <Paper sx={{
        marginTop: '-65px',
        borderRadius: '8px',
        zIndex: 1111
    }}
      elevation={7}>
        <Card sx={{
        background: 'linear-gradient(195deg, rgb(73, 163, 241), rgb(26, 115, 232))',
        borderRadius: '8px',
        padding: '25px',
        color: 'white',
        fontSize: '18px',
        fontWeight: '700'
        }}>
            {/* Authors Table */}

           {props.title}

        </Card>
    </Paper>
  )
}

export default CardHeader