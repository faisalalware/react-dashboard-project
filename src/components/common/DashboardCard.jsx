import React from "react";
import { Paper } from "@mui/material";

const DashboardCard = ({ sx = {}, ...props }) => {
  const _sx = {
    borderRadius: "10px",
    backgroundColor: "background.paper",
    boxShadow: "0px 5px 10px #d9dbde",
    px: {xs:2, sm:3.5},
    py: 3.5,
    height: "100%",
    display: "flex",
    flexDirection: "column",
    position: "relative",
    ...sx
  };

  return (
    <Paper sx={_sx} {...props} />
  );
};

export default DashboardCard;