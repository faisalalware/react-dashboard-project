import React from "react";
import Login from "../../components/Auth/Login";
import { Stack } from "@mui/material";
import FooterLight from "../../components/Auth/FooterLight";
import HeaderDark from "../../components/Auth/HeaderDark";

class LoginContainer extends React.Component {

    render() {
        return (
            <Stack sx={{
                minHeight: "100vh",
                backgroundImage: 'url(/Images/Auth/login-background.jpeg)',
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
                backgroundPosition: "center",
            }}>
                <Stack sx={{
                    flexDirection: "column",
                    height: "100vh",
                    justifyContent: "space-between",
                }}>
                    <Stack>
                        <HeaderDark />
                    </Stack>
                    <Stack>
                        <Login />
                    </Stack>
                    <Stack>
                        <FooterLight />
                    </Stack>
                </Stack>
            </Stack>
        )
    }
}

export default LoginContainer;