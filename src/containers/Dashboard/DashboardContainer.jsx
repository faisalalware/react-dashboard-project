import React from "react";
import Dashboard from "../../components/Dashboard/Dashboard";
import Header from "../../components/Layout/Header";

class DashboardContainer extends React.Component{

    render() {
        return (
            <>
            <Header title="Dashboard"/>
            <Dashboard />
            </>
        )
    }
}

export default DashboardContainer;