import React from "react";
import Header from "../../components/Layout/Header";
import Footer from "../../components/Layout/Footer";
import Sidebar from "../../components/Layout/Sidebar";
import { Stack } from "@mui/material";
import { Outlet } from "react-router-dom";

class LayoutContainer extends React.Component{

    render() {
        return (
            <Stack direction="row" sx={{ minHeight: "100vh" }}>
                <Sidebar />
                <Stack
                    sx={{
                        flex: 1,
                        // backgroundColor: {
                        //     xs: "background.paper",
                        //     md: "grey.100"
                        // },
                        backgroundColor: "grey.100"
                        // width: isDrawerExpanded ? "calc(100% - 240px)" : "100%",
                    }}>
                        {/* <Header /> */}
                    <Stack
                        sx={{
                            position: "relative",
                            flex: 1,
                            maxWidth: "100%",
                            px: { xs: 2, md: 3 },
                            py: 0,
                            pb: { xs: 2, md: 3 },
                        }}>                        
                        <Outlet /> 
                    </Stack>
                    <Footer />
                </Stack>
            </Stack>
        )
    }
}

export default LayoutContainer;