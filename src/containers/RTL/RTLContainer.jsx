import React from "react";
import RTL from "../../components/RTL/RTL";
import Header from "../../components/Layout/Header";

class RTLContainer extends React.Component {

    render() {
        return (
            <>
                <Header title="RTL" />
                <RTL />
            </>
        )
    }
}

export default RTLContainer;