import React from "react";
import Profile from "../../components/Profile/Profile";
import Header from "../../components/Layout/Header";

class ProfileContainer extends React.Component {

    render() {
        return (
            <>
                <Header title="Profile" />
                <Profile />
            </>
        )
    }
}

export default ProfileContainer;