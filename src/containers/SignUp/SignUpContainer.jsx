import React from "react";
import { Stack } from "@mui/material";
import FooterDark from "../../components/Auth/FooterDark";
import HeaderLight from "../../components/Auth/HeaderLight";
import SignUp from "../../components/Auth/SignUp";

class SignUpContainer extends React.Component {

    render() {
        return (
            <Stack sx={{
                backgroundColor: "grey.100",
            }}>
                <Stack sx={{
                    margin: "15px",
                    minHeight: "35vh",
                    backgroundImage: 'url(/Images/Auth/signup-background.jpeg)',
                    backgroundRepeat: "no-repeat",
                    backgroundSize: "cover",
                    backgroundPosition: "center",
                    borderRadius: "15px",
                }}>
                    <Stack sx={{

                    }}>
                        <HeaderLight />
                    </Stack>
                </Stack>
                <Stack sx={{
                    backgroundColor: "grey.100",
                    flexDirection: "column",
                    height: "61vh",
                    justifyContent: "space-between",
                }}>
                    <Stack sx={{
                        marginTop: "-125px"
                    }}>
                        <SignUp />
                    </Stack>
                    <Stack>
                        <FooterDark />
                    </Stack>
                </Stack>
            </Stack>
        )
    }
}

export default SignUpContainer;