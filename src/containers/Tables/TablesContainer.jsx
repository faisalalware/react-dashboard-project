import React from "react";
import AuthorsTable from "../../components/Tables/AuthorsTable";
import ProjectTable from "../../components/Tables/ProjectTable";
// import CardHeader from '../../components/common/CardHeader'
import Stack from "@mui/material/Stack";
import Header from "../../components/Layout/Header";

class TablesContainer extends React.Component {

    render() {
        return (
            <>
                <Header title="Tables" />
                <Stack sx={{
                    mt: 6,
                    mb: 10
                }}>
                    <AuthorsTable />
                </Stack>
                <Stack>
                    <ProjectTable />
                </Stack>
            </>
        )
    }
}

export default TablesContainer;