import React from "react";
import Billing from "../../components/Billing/Billing";
import Header from "../../components/Layout/Header";

class BillingContainer extends React.Component{

    render() {
        return (
            <>
            <Header title="Billing" />
            <Billing />
            </>
        )
    }
}

export default BillingContainer;