import React from "react";
import Notifications from "../../components/Notifications/Notifications";
import Header from "../../components/Layout/Header";

class NotificationsContainer extends React.Component {

    render() {
        return (
            <>
                <Header title="Notifications" />
                <Notifications />
            </>
        )
    }
}

export default NotificationsContainer;